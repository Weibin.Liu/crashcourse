package basics;
import java.lang.Math;

public class MathOperators {
	
	public static void main(String[] args) {
		int num1 = 5;
		int num2 = 2;
		
		int addNums = num1 + num2;
		System.out.println(num1 + " + " + num2 + " = " + addNums);
		
		int subtractNums = num1 - num2;
		System.out.println(num1 + " - " + num2 + " = " + subtractNums);
		
		int multiplyNums = num1 * num2;
		System.out.println(num1 + " * " + num2 + " = " + multiplyNums);
		
		int divideNums = num1 / num2;
		System.out.println(num1 + " / " + num2 + " = " + divideNums);

		double divideNumsDouble = num1 / num2;
		System.out.println(num1 + " / " + num2 + " = " + divideNumsDouble);

		double divideNumsDoubleCast = (double) num1 / (double) num2;
		System.out.println(num1 + " / " + num2 + " = " + divideNumsDoubleCast);
		
		double exponentNums = Math.pow(num1, num2);
		System.out.println(num1 + " ^ " + num2 + " = " + exponentNums);
		
		double moduloNums = num1 % num2;
		System.out.println(num1 + " % " + num2 + " = " + moduloNums);
	}
}
